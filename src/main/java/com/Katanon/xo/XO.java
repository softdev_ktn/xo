/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.Katanon.xo;

import java.util.*;

public class XO {

    private final static int SIZE = 3;
    private static char[][] board = new char[SIZE][SIZE];
    private static char currentPlayer = 'X';

    public static void main(String[] args) {
        printWelcome();
        createBoard();
        while (true) {
            printBoard();
            move();
            if(results()){
                break;
            }
            playerSwap();
        }
            
    }

    public static void printWelcome() {
        System.out.println("Welcome to XO!!");
    }

    public static void createBoard() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                board[i][j] = '-';
            }
        }
    }

    public static void printBoard() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println("");
        }
    }

    public static void move() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Player " + currentPlayer + ",Enter your move (row and column):");
            int row = sc.nextInt();
            int col = sc.nextInt();
            if (row <= 0 || row > SIZE || col <= 0 || col > SIZE) {
                System.out.println("This position is outside of the board!");
            } else if (board[row - 1][col - 1] != '-') {
                System.out.println("Player is already in that spot!");
            } else {
                board[row - 1][col - 1] = currentPlayer;
                break;
            }
        }
    }

    public static void playerSwap() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    //check win
    public static boolean checkWinner() {
        // check row
        if ((board[0][0] == currentPlayer && board[0][1] == currentPlayer && board[0][2] == currentPlayer)
                || (board[1][0] == currentPlayer && board[1][1] == currentPlayer && board[1][2] == currentPlayer)
                || (board[2][0] == currentPlayer && board[2][1] == currentPlayer && board[2][2] == currentPlayer)) {
            return true;
        } // check col
        else if ((board[0][0] == currentPlayer && board[1][0] == currentPlayer && board[2][0] == currentPlayer)
                || (board[0][1] == currentPlayer && board[1][1] == currentPlayer && board[2][1] == currentPlayer)
                || (board[0][2] == currentPlayer && board[1][2] == currentPlayer && board[2][2] == currentPlayer)) {
            return true;
        } // check X
        else if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer
                || board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    public static boolean checkDraw() {
        // check draw
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean results() {
        if (checkWinner()) {
            printBoard();
            System.out.println("Player " + currentPlayer + " win!!!");
            return true;
        } else if (checkDraw()) {
            printBoard();
            System.out.println("Draw!!!");
            return true;
        }
        return false;
    }
}
